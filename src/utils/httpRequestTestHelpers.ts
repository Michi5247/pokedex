/**
 * These are helper functions I have written in past to make testing
 * services, that use httpClient, more ergonomic
 */

import {HttpTestingController} from "@angular/common/http/testing";
import {flushMicrotasks} from "@angular/core/testing";
import httpStatusCodes from "./httpStatusCodes";
import {HttpHeaders} from "@angular/common/http";

type httpVerb = 'GET' | 'POST' | 'DELETE' | 'OPTIONS' | 'PUT' | 'PATCH';

interface HttpRequestExpectation {
  URL: string;
  method?: httpVerb;
  // tslint:disable-next-line:ban-types
  flush?: ArrayBuffer | Blob | string | number | Object | (string | number | Object | null);
  flushStatusCode?: number;
  reqBody?: any | null;
}

type flushOptions = {
  headers?: HttpHeaders | {
    [name: string]: string | string[];
  };
  status?: number;
  statusText?: string;
};

type expectationFormat = HttpRequestExpectation | string;

/**
 * Finds the corresponding message of common HTTP status codes
 */
function getStatusCodeMessage(code: number): string {
  if (code in httpStatusCodes) {
    // @ts-ignore
    return httpStatusCodes[code].message;
  } else {
    throw new Error("Unknown HTTP Status Code");
  }
}

function runSequentialRequests(httpTestCont: HttpTestingController, expectations: HttpRequestExpectation[]) {

  for (const expectation of expectations) {

    const request = httpTestCont.expectOne(expectation.URL, "expect request for '" + expectation.URL + "'");

    const flushOpts: flushOptions = {};

    if (expectation.flushStatusCode) {
      flushOpts.status = expectation.flushStatusCode;
      flushOpts.statusText = getStatusCodeMessage(flushOpts.status);
    }

    request.flush(expectation.flush || "", flushOpts);
    flushMicrotasks();

    if (expectation.method) {
      expect(request.request.method).toBe(expectation.method, "request not with expected method");
    }

    if (expectation.reqBody) {
      expect(request.request.body).toBe(expectation.reqBody);
    }
  }
}

function convertToExpectation(ex: string | HttpRequestExpectation): HttpRequestExpectation {
  if (typeof ex === 'string') {
    return {URL: ex};
  }
  if (typeof ex === 'object') {
    if ('URL' in ex) {
      return ex;
    }
  }
  throw new Error("Invalid Expectation format");
}

/**
 * Expects a sequence of http requests in a format described by expectations[].
 * Has to be run inside fakeAsync
 */
export function expectSequentialRequests(httpTestController: HttpTestingController, expectations: expectationFormat[]) {

  const unifiedExpectations = expectations.map(convertToExpectation);

  runSequentialRequests(httpTestController, unifiedExpectations);
}

/**
 * Expects a http request in the format described by expectation
 * Has to be run inside fakeAsync
 */
export function expectRequest(httpTestController: HttpTestingController, expectation: expectationFormat) {
  const unifiedExpectation = convertToExpectation(expectation);

  runSequentialRequests(httpTestController, [unifiedExpectation]);
}
