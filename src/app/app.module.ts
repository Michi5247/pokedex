import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {HttpClientModule} from "@angular/common/http";
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {ListPageComponent} from './list-page/list-page.component';
import {MatPaginatorModule} from "@angular/material/paginator";
import {MatToolbarModule} from "@angular/material/toolbar";
import {MatButtonModule} from "@angular/material/button";
import {DetailsPageComponent} from './details-page/details-page.component';
import {MatCardModule} from "@angular/material/card";
import {AppRouteReuseStrategy} from "./AppRouteReuseStrategy";
import {RouteReuseStrategy} from "@angular/router";

@NgModule({
  declarations: [
    AppComponent,
    ListPageComponent,
    DetailsPageComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    BrowserAnimationsModule,
    MatPaginatorModule,
    MatToolbarModule,
    MatButtonModule,
    MatCardModule
  ],
  providers: [
    {provide: RouteReuseStrategy, useClass: AppRouteReuseStrategy}
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
