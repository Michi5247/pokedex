import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {PokeapiService} from "../pokeapi/pokeapi.service";
import {PokeapiPokemonResponse} from "../pokeapi/PokapiPokemonResponse";
import {HttpErrorResponse} from "@angular/common/http";
import {PokeapiEvolutionChainResponse} from "../pokeapi/PokeapiEvolutionChainResponse";
import {SubSink} from "subsink";

interface Evolution {
  id: string,
  name: string,
  url: string,
  image: string
}

@Component({
  selector: 'app-details-page',
  templateUrl: './details-page.component.html',
  styleUrls: ['./details-page.component.scss']
})
export class DetailsPageComponent implements OnInit, OnDestroy {

  public pokemon?: PokeapiPokemonResponse;
  public errorType: 'NONE' | '404' = 'NONE';
  public evolutions: Evolution[] = [];
  private subs = new SubSink();

  constructor(private route: ActivatedRoute, private pokeapi: PokeapiService) {
  }

  /**
   * Transforms the nested data structure the PokeAPI returns into a simple array
   */
  private static flattenEvolutionChain(evolutionChain: PokeapiEvolutionChainResponse): Evolution[] {
    const flattenedChain = [];
    let evolutionTemp = evolutionChain.chain;

    while (evolutionTemp.species) {
      flattenedChain.push({
        id: PokeapiService.extractIDFromPokeapiURI(evolutionTemp.species.url),
        name: evolutionTemp.species.name,
        url: evolutionTemp.species.url,
        image: PokeapiService.getPokemonSpriteByURL(evolutionTemp.species.url)
      });

      if (evolutionTemp.evolves_to?.[0]) {
        evolutionTemp = evolutionTemp.evolves_to[0];
      } else {
        break;
      }
    }

    return flattenedChain;
  }

  ngOnInit(): void {

    // In theory a race condition
    this.subs.sink = this.route.paramMap.subscribe(async (params) => {
        const id = params.get("id");

        try {
          if (!id) {
            this.errorType = '404';
          } else {
            this.errorType = 'NONE';
            await this.loadPokemon(id);
          }
        } catch (e) {
          if (e instanceof HttpErrorResponse && e.status === 404) {
            this.errorType = '404'
          }
        }
      }
    )
  }

  async loadPokemon(id: string) {
    this.pokemon = await this.pokeapi.getPokemonDetailsById(id);

    const species = await this.pokeapi.getPokemonSpeciesDetails(this.pokemon.species.url);

    const evolutionChain = await this.pokeapi.getEvolutionChainDetails(species.evolution_chain.url)

    // Flattening the evolution chain
    this.evolutions = DetailsPageComponent.flattenEvolutionChain(evolutionChain);
  }

  ngOnDestroy(): void {
    this.subs.unsubscribe();
  }
}
