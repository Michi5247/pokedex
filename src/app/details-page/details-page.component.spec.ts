import {ComponentFixture, fakeAsync, TestBed} from '@angular/core/testing';

import {DetailsPageComponent} from './details-page.component';
import {MatCardModule} from "@angular/material/card";
import {ActivatedRoute} from "@angular/router";
import {Subject} from "rxjs";
import {HttpClient} from "@angular/common/http";
import {HttpClientTestingModule, HttpTestingController} from "@angular/common/http/testing";
import {NoopAnimationsModule} from "@angular/platform-browser/animations";
import {expectRequest, expectSequentialRequests} from "../../utils/httpRequestTestHelpers";
import {environment} from "../../environments/environment";
import * as ivysaurResponse from "../../app/pokeapi/ivysaurResponse.json";
import * as ivysaurSpeciesResponse from "../../app/pokeapi/ivysaurSpeciesResponse.json";
import * as ivysaurEvolutionResponse from "../../app/pokeapi/ivysaurEvolutionChainResponse.json";

describe('DetailsPageComponent', () => {
  let component: DetailsPageComponent;
  let fixture: ComponentFixture<DetailsPageComponent>;

  let httpClient: HttpClient;
  let httpTestCont: HttpTestingController;

  const activatedRouteSubject = new Subject();

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [DetailsPageComponent],
      imports: [MatCardModule, NoopAnimationsModule, HttpClientTestingModule],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: {
            paramMap: activatedRouteSubject
          }
        }
      ]
    })
      .compileComponents();

    httpClient = TestBed.inject(HttpClient);
    httpTestCont = TestBed.inject(HttpTestingController);
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailsPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should display information about a Pokemon', fakeAsync(() => {

    activatedRouteSubject.next(new Map([['id', 2]]));

    expectSequentialRequests(httpTestCont, [
      {
        URL: environment.pokeapiEndpoint + 'pokemon/2',
        method: 'GET',
        flush: ivysaurResponse
      },
      {
        URL: environment.pokeapiEndpoint + 'pokemon-species/2',
        method: 'GET',
        flush: ivysaurSpeciesResponse
      },
      {
        URL: environment.pokeapiEndpoint + 'evolution-chain/2',
        method: 'GET',
        flush: ivysaurEvolutionResponse
      }
    ])
    httpTestCont.verify();

    fixture.detectChanges();

    const compiled = fixture.nativeElement as HTMLElement;

    expect(compiled.querySelector('h1')?.textContent)
      .toContain('ivysaur');

    expect(compiled.querySelector('ul:first-of-type li:last-child')?.textContent)
      .toContain('chlorophyll (hidden)');

    expect(compiled.querySelector('dl:first-of-type dt:first-child')?.textContent)
      .toContain('hp');

    expect(compiled.querySelector('ul:last-of-type')?.textContent)
      .toContain('leech-seed');

    expect(compiled.querySelector('mat-card.possible-evolutions a:nth-of-type(2)')?.textContent)
      .toContain('ivysaur');

  }))

  it('should display a 404 message when an invalid id is given', fakeAsync(() => {

    activatedRouteSubject.next(new Map([['id', 'abc']]));

    expectRequest(httpTestCont, {
      URL: environment.pokeapiEndpoint + 'pokemon/abc',
      method: 'GET',
      flushStatusCode: 404,
      flush: 'Not Found'
    })

    httpTestCont.verify();

    fixture.detectChanges();

    const compiled = fixture.nativeElement as HTMLElement;
    expect(compiled.querySelector('h1')?.textContent)
      .toContain('404')
  }))

  it('should display a 404 message when no id is given', fakeAsync(() => {
    activatedRouteSubject.next(new Map([['id', '']]));

    fixture.detectChanges();

    const compiled = fixture.nativeElement as HTMLElement;
    expect(compiled.querySelector('h1')?.textContent)
      .toContain('404')
  }))
});
