import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {PokeapiPaginationResult, PokeapiService} from "../pokeapi/pokeapi.service";

@Component({
  selector: 'app-list-page',
  templateUrl: './list-page.component.html',
  styleUrls: ['./list-page.component.scss']
})
export class ListPageComponent implements OnInit {

  public readonly defaultPageSize = 10;
  public length = 0;
  public pokemonArray: PokeapiPaginationResult[] = [];

  @ViewChild('paginator', {read: ElementRef})
  private paginatorElement?: ElementRef;

  constructor(private pokeapi: PokeapiService) {
  }

  ngOnInit(): void {
    this.setPage(0, this.defaultPageSize);
  }

  async setPage(pageNumber: number, pageSize: number): Promise<void> {
    const response = await this.pokeapi.getPokemonListPage(pageNumber, pageSize);

    this.length = response.count;
    this.pokemonArray = response.results;
  }

  getSpriteForURI(uri: string) {
    return PokeapiService.getPokemonSpriteByURL(uri);
  }

  scrollToTop() {
    window.scrollTo(0, 0);
    (this.paginatorElement?.nativeElement as HTMLElement).focus();
  }

  extractIdFromURI(uri: string) {
    return PokeapiService.extractIDFromPokeapiURI(uri);
  }
}
