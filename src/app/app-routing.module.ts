import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {ListPageComponent} from "./list-page/list-page.component";
import {DetailsPageComponent} from "./details-page/details-page.component";

const routes: Routes = [
  {
    path: '',
    component: ListPageComponent
  },
  {
    path: 'pokemon/:id',
    component: DetailsPageComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {scrollPositionRestoration: "enabled"})],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
