// Heavily inspired by https://stackoverflow.com/questions/41280471/how-to-implement-routereusestrategy-shoulddetach-for-specific-routes-in-angular

import {ActivatedRouteSnapshot, DetachedRouteHandle, RouteReuseStrategy} from "@angular/router";

export class AppRouteReuseStrategy implements RouteReuseStrategy {
  routesToCache = new Set(
    ['']
  );

  storedRouteHandles = new Map<string, DetachedRouteHandle>();

  // Decides if the route should be stored
  shouldDetach(route: ActivatedRouteSnapshot): boolean {
    if (route.routeConfig === null || route.routeConfig.path === undefined) {
      return false;
    }
    return this.routesToCache.has(route.routeConfig?.path);
  }

  //Store the information for the route we're destructing
  store(route: ActivatedRouteSnapshot, handle: DetachedRouteHandle): void {
    if (route.routeConfig !== null && route.routeConfig.path !== undefined) {
      this.storedRouteHandles.set(route.routeConfig.path, handle);
    }
  }

//Return true if we have a stored route object for the next route
  shouldAttach(route: ActivatedRouteSnapshot): boolean {
    if (route.routeConfig !== null && route.routeConfig.path !== undefined) {
      return this.storedRouteHandles.has(route.routeConfig.path);
    } else {
      return false;
    }
  }

  //If we returned true in shouldAttach(), now return the actual route data for restoration
  retrieve(route: ActivatedRouteSnapshot): DetachedRouteHandle {
    return this.storedRouteHandles.get(<string>route?.routeConfig?.path) as DetachedRouteHandle;
  }

  //Reuse the route if we're going to and from the same route
  shouldReuseRoute(future: ActivatedRouteSnapshot, curr: ActivatedRouteSnapshot): boolean {
    return future.routeConfig === curr.routeConfig;
  }
}
