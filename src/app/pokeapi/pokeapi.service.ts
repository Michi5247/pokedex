import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {environment} from "../../environments/environment";
import {PokeapiPokemonResponse} from "./PokapiPokemonResponse";
import {PokeapiPokemonSpeciesResponse} from "./PokeapiPokemonSpeciesResponse";
import {PokeapiEvolutionChainResponse} from "./PokeapiEvolutionChainResponse";

export interface PokeapiPaginationResult {
  name: string;
  url: string;
}

export interface PokeapiPaginationResponse {
  count: number;
  next: string | null;
  previous: string | null;
  results: PokeapiPaginationResult[];
}

@Injectable({
  providedIn: 'root'
})
export class PokeapiService {

  constructor(private http: HttpClient) {
  }

  /**
   * @example
   * extractIDFromPokeapiURI("https://pokeapi.co/api/v2/pokemon/295/");
   * // Returns 295
   */
  public static extractIDFromPokeapiURI(pokemonURI: string): string {
    const url = new URL(pokemonURI);

    // Matches last part of URL, ignores trailing '/'
    const matcher = /([^\/]+)\/?$/;
    const match = url.pathname.match(matcher);

    if (match === null || !match[1]) {
      throw new TypeError("Invalid PokeAPI URI");
    }
    return match[1];
  }

  public async getPokemonListPage(page: number, limit = 30): Promise<PokeapiPaginationResponse> {

    return this.http.get<PokeapiPaginationResponse>(environment.pokeapiEndpoint + "pokemon", {
      params: {
        offset: page * limit,
        limit
      }
    }).toPromise();
  }

  public async getPokemonDetailsById(id: string): Promise<PokeapiPokemonResponse> {

    return this.http.get<PokeapiPokemonResponse>(environment.pokeapiEndpoint + "pokemon/" + encodeURIComponent(id))
      .toPromise();
  }

  public async getEvolutionChainDetails(evolutionChainURI: string): Promise<PokeapiEvolutionChainResponse> {
    const id = PokeapiService.extractIDFromPokeapiURI(evolutionChainURI);

    return this.http.get<PokeapiEvolutionChainResponse>(environment.pokeapiEndpoint + "evolution-chain/" + encodeURIComponent(id))
      .toPromise();
  }

  public async getPokemonSpeciesDetails(pokemonSpeciesURI: string): Promise<PokeapiPokemonSpeciesResponse> {
    const id = PokeapiService.extractIDFromPokeapiURI(pokemonSpeciesURI);

    return this.http.get<PokeapiPokemonSpeciesResponse>(environment.pokeapiEndpoint + "pokemon-species/" + encodeURIComponent(id))
      .toPromise();
  }

  public static getPokemonSpriteByURL(pokemonURI: string): string {
    const id = PokeapiService.extractIDFromPokeapiURI(pokemonURI);

    return `https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/${id}.png`
  }
}
