import {fakeAsync, TestBed} from '@angular/core/testing';

import {PokeapiService} from './pokeapi.service';
import {HttpClientTestingModule, HttpTestingController} from "@angular/common/http/testing";
import {HttpClient} from "@angular/common/http";
import {expectRequest} from "../../utils/httpRequestTestHelpers";
import {environment} from "../../environments/environment";
import * as listResponse from '../../app/pokeapi/listPage2Limit5Response.json';

describe('PokeapiService', () => {
  let service: PokeapiService;

  let httpClient: HttpClient;
  let httpTestCont: HttpTestingController;


  beforeEach(() => {
    TestBed.configureTestingModule({imports: [HttpClientTestingModule]});
    service = TestBed.inject(PokeapiService);

    httpClient = TestBed.inject(HttpClient);
    httpTestCont = TestBed.inject(HttpTestingController);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('extractIDFromPokeapiURI should correctly extract the id', () => {
    expect(PokeapiService.extractIDFromPokeapiURI('https://pokeapi.co/api/v2/pokemon/123'))
      .toBe('123');

    expect(PokeapiService.extractIDFromPokeapiURI('https://pokeapi.co/api/v2/pokemon/123/'))
      .toBe('123');

    expect(() => {
      PokeapiService.extractIDFromPokeapiURI('https://pokeapi.co/')
    }).toThrow();
  })

  it('getPokemonListPage should make appropriate requests', fakeAsync(() => {
    const response = service.getPokemonListPage(1, 5);

    expectRequest(httpTestCont, {
      URL: environment.pokeapiEndpoint + 'pokemon?offset=5&limit=5',
      method: 'GET',
      flush: listResponse
    })

    httpTestCont.verify();
  }))
});
